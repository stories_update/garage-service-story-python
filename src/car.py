class Car:

    CLASSIC_CAR_ABBREVIATION = "C"
    SIMPLE_CAR_ABBREVIATION = "S"

    def __init__(self, manufacturer, model, year, is_classic):
        self.manufacturer = manufacturer
        self.model = model
        self.year = year
        self.is_classic = is_classic

    def __repr__(self):
        is_classic = (
            Car.CLASSIC_CAR_ABBREVIATION
            if self.is_classic
            else Car.SIMPLE_CAR_ABBREVIATION
        )
        return (
            f"Manufacturer - {self.manufacturer}, "
            f"model - {self.model}, year - {self.year}, "
            f"is_classic - {is_classic}"
        )
